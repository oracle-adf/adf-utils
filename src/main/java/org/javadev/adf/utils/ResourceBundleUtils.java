package org.javadev.adf.utils;

 import java.util.MissingResourceException;  
 import java.util.ResourceBundle;  
 import javax.faces.context.FacesContext; 

public class ResourceBundleUtils {
    
    private static ADFLogger LOGGER = ADFLogger.createADFLogger(ResourceBundleUtils.class);
    private static String message_not_found = " not found in resource bundle";
    
    public static String getResourceBundleKey(String resourceBundlePath, String key) {  
      ResourceBundle bundle = getResourceBundle(resourceBundlePath);  
      return (String)getResourceBundleKey(bundle, key);  
    }  
    
    private static FacesContext getFacesContext() {  
      return FacesContext.getCurrentInstance();  
    }  
    
    private static ResourceBundle getResourceBundle(String resourceBundlePath) {  
      return ResourceBundle.getBundle(resourceBundlePath, getFacesContext().getViewRoot().getLocale());  
    }  

    private static Object getResourceBundleKey(ResourceBundle resourceBundle, String key) {   
      try {  
        return (Object)resourceBundle.getString(key);  
      } catch (MissingResourceException mrExp) {  
        LOGGER.severe(key + message_not_found);
        throw new RuntimeException(key + message_not_found);
      }  
    } 
    
} // The End of Class;
