package org.javadev.adf.utils;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;

import oracle.binding.BindingContainer;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowIterator;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;
import oracle.jbo.uicli.binding.JUCtrlHierBinding;
import oracle.jbo.uicli.binding.JUCtrlListBinding;


public class IteratorUtils {
   
    private static ADFLogger _logger = ADFLogger.createADFLogger(IteratorUtils.class);

    public static void printIteratorInfo(String iteratorName){
        
        try{
                
            System.out.println("----------------------------");
            System.out.println("------- IteratorInfo -------!!!");
    
            DCIteratorBinding iterator = ADFUtils.findIterator(iteratorName);
    
            // Определить количество записей
    
            RowSetIterator rsi = iterator.getRowSetIterator();
    
    
            System.out.println("||||||||||||||||||||||||||");
            System.out.println("Records Count : " + rsi.getRowCount());
    
    
            // Данные из строки, на которую указывает итератор
    
            Row r = rsi.getCurrentRow();

            int numAttrs = rsi.getCurrentRow().getAttributeCount();
                
                
            System.out.println("AttributeCount : " + numAttrs);
            System.out.println("Current Row Index: " + rsi.getCurrentRowIndex());
                
            String rowDataStr = "";

            
            for (int columnNo = 0; columnNo < numAttrs; columnNo++){
                 Object attrData = rsi.getCurrentRow().getAttribute(columnNo);
                 rowDataStr += (attrData + "\t");
              }

            System.out.println("Values) " + rowDataStr);
            
            System.out.println("||||||||||||||||||||||||||");

        
        } catch (Exception ex){
            System.out.println("!!! Exception :: printIteratorInfo " + ex.toString());
        }
    }
    
    
    public static void printIteratorValues(String iteratorName){
        
        try{
            
            DCIteratorBinding iterator = ADFUtils.findIterator(iteratorName);

                   // Определить количество записей

                   RowSetIterator rsi = iterator.getRowSetIterator();

            rsi.reset();

            while (rsi.getCurrentRow() != null) {

                String rowDataStr = "";
                int numAttrs = rsi.getCurrentRow().getAttributeCount();
               
                for (int columnNo = 0; columnNo < numAttrs; columnNo++){
                     Object attrData = rsi.getCurrentRow().getAttribute(columnNo);
                     rowDataStr += (attrData + "\t");
                  }
                
                System.out.println(rsi.getCurrentRowIndex() + ") " + rowDataStr);
                rsi.next();

            }
            
        } catch (Exception ex){
            System.out.println("!!! Exception :: printIteratorValues " + ex.toString());
        }
        
    }
    
    // GET VALUE FROM ITERATOR
    
        public static Object getValFromIterator(String iteratorName, String attrName){
            try { 
               return (Object) ADFUtils.findIterator(iteratorName).getCurrentRow().getAttribute(attrName); 
            } catch (Exception ex) {
                
                new GetValueFromIteratorException(iteratorName, attrName);
                new GetValueFromIteratorException(ex);
                return "";
            }
        }
        
// БЫЛО !!!
        
//    public static Object getValFromIterator(String iteratorName, String attrName){
//        
//        try { 
//           DCIteratorBinding dcItteratorBindings =  ADFUtils.findIterator(iteratorName);
//           ViewObject vo = dcItteratorBindings.getViewObject();
//           Row rowSelected = vo.getCurrentRow();
//           return (rowSelected.getAttribute(attrName)); 
//        } catch (Exception e) {
//            System.out.println("!!! Exception :: getValFromIteratorByIteratorName: " + e.getMessage());
//            return "";
//        }
//    }
    
 // БЫЛО END!
    
//    
//    public static String getValFromIteratorVer2(String vo, String attrName) {
//        try {
//            BindingContainer bindings = BindingContext.getCurrent().getCurrentBindingsEntry();
//            JUCtrlHierBinding fclb = (JUCtrlHierBinding)bindings.get(vo);
//            return fclb.getRowIterator().getCurrentRow().getAttribute(attrName).toString();
//        } catch (Exception e) {
//            System.out.println("*GetValFromIterator*" + e.getMessage());
//            return "";
//        }
//    }

//    public static String getValFromIteratorList(String vo, String attrName) {
//        try {
//            BindingContainer bindings = BindingContext.getCurrent().getCurrentBindingsEntry();
//            JUCtrlListBinding fclb = (JUCtrlListBinding)bindings.get(vo);
//            return fclb.getRowIterator().getCurrentRow().getAttribute(attrName).toString();
//        } catch (Exception e) {
//            System.out.println("*GetValFromIterator*[" + vo + "]" + e.getMessage());
//            return "";
//        }
//    }
    
    
    //  Set Iterator Position
    
    public static void setIteratorPositionByAttrValue(String iteratorName, String attrName, String attrValue) {
        try {
            DCIteratorBinding iterator = ADFUtils.findIterator(iteratorName);
            RowSetIterator rsi = iterator.getRowSetIterator();
            
            rsi.reset();
            
            Boolean found  = false;

            while (rsi.getCurrentRow() != null) {
                for (int columnNo = 0; columnNo < rsi.getRowCount(); columnNo++){
                     Object attrData = rsi.getCurrentRow().getAttribute(attrName);
                    
                    if (attrData.equals(attrValue)){
                       found = true;
                     // System.out.println("attrData " + attrData);
                     return;}
                  }
                
                if(found){
                    return; 
                }
               // System.out.println(rsi.getCurrentRowIndex() + ") " + rowDataStr);
                rsi.next();
            }
            rsi.reset();
            
        } catch (Exception ex) {
            System.out.println("!!! Exception :: setIteratorPosition " + ex.toString());
        }
    }
    
    
    public static void setCurrentRow(String vo, String keyValue) {
        String trap = "";
        try {
            trap = "1";
            BindingContainer bindings = BindingContext.getCurrent().getCurrentBindingsEntry();
            trap = "2";
            JUCtrlListBinding fclb = (JUCtrlListBinding)bindings.get(vo);
            trap = "3";
            Key k = new Key(new Object[] { keyValue });
            trap = "4";
            RowIterator ri = fclb.getRowIterator();
            trap = "4.1";
            Row[] sRow = ri.findByKey(k, 1);
            trap = "5";
            fclb.getRowIterator().setCurrentRow(sRow[0]);
        } catch (Exception ex) {
            System.out.println("!!! Exception || BeanIndex.setCurrentRow " + ex.getMessage() + " trap:" + trap);
        }
    }
    
} // THe End of Class;
