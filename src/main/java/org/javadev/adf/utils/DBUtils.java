package org.javadev.adf.utils;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCDataControl;

import oracle.jbo.server.ApplicationModuleImpl;
import oracle.jbo.server.DBTransaction;

import org.javadev.adf.utils.impl.vo.CONSTANTS_VO;

public class DBUtils {
        
    public static DBTransaction getDBTransaction(){
        ApplicationModuleImpl am = (ApplicationModuleImpl)ADFUtils.getApplicationModuleForDataControl(CONSTANTS_VO.AppModuleName);
        DBTransaction db = null;
        db = am.getDBTransaction();            
        return db;
    }
    
    
    // For Information
    
//    public static DBTransaction getDBTransaction(){
//        BindingContext bindingContext = BindingContext.getCurrent();
//        DCDataControl dc = bindingContext.getDefaultDataControl();
//        AppModuleImpl am = (AppModuleImpl) dc.getDataProvider();
//        DBTransaction dbT = am.getDBTransaction();
//        return dbT;
//    }
    

    
} // The End of Class;
