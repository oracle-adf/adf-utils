package org.javadev.adf.utils;
// import org.javadev.proj.model.AppModuleImpl;

public class AppModuleUtils {
    
    private static String DefaultAppModule = "AppModuleDataControl";
    

    public static AppModuleImpl getAppModuleImpl(){
        return (AppModuleImpl) ADFUtils.getApplicationModuleForDataControl(DefaultAppModule);
    }

} // The End of Class;
